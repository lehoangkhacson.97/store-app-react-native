import React, { useEffect } from 'react'
import { FlatList } from 'react-native-gesture-handler'
import { StyleSheet, Text, View, Image } from 'react-native'
import { connect } from 'react-redux'

import ProductListItem from '../components/ProductListItem'
import { getProduct } from '../actions/product'

const Category = (props: any) => {
  const { route, getProduct, product } = props
  const { restaurant } = route.params
  const { name, address, _id, image } = restaurant

  useEffect(() => {
    getProduct(_id)
  }, [])

  return (
    <View>
      <Image
        style={styles.img}
        source={{
          uri: image,
        }}
      />
      <View style={styles.info}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.address}>{address}</Text>
      </View>
      <FlatList
        data={product.data}
        renderItem={({ item }) => <ProductListItem data={item} />}
        keyExtractor={(item: any) => item._id}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingTop: 16,
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: 8,
  },
  img: {
    height: 200,
  },
  info: {
    padding: 8,
  },
  name: {
    fontWeight: '600',
    fontSize: 18,
  },
  address: {
    color: '#808080',
  },
})

const mapStateToProps = (state: any) => {
  return {
    product: state.product,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProduct: (restaurantId: any) => dispatch(getProduct(restaurantId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)
