import {
  GET_PRODUCT,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_ERROR,
} from '../constants'

const initialState: any = {
  loading: false,
  data: [],
  error: '',
}

export default function reducer(state = initialState, actions: any) {
  switch (actions.type) {
    case GET_PRODUCT: {
      return {
        ...initialState,
        loading: true,
      }
    }
    case GET_PRODUCT_SUCCESS: {
      const { data } = actions.payload
      return {
        ...initialState,
        data,
        loading: false
      }
    }
    case GET_PRODUCT_ERROR: {
      return {
        ...initialState,
        loading: false,
        error: actions.payload,
      }
    }
    default:
      return state
  }
}
