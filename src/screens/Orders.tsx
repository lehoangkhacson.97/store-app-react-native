import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
// import { Toast } from '@ant-design/react-native'

export default function Orders() {
  return (
    <View style={styles.container}>
      <View style={styles.loading}>
        <Text>Toast loading</Text>
        {/* {Toast.loading('loading...', 10, () => {
          console.log('on close loading')
        })} */}
      </View>
      <Text>Orders</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    borderColor: '#f34000',
    borderStyle: 'solid',
  },
})
