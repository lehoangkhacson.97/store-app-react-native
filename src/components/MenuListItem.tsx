import React from 'react'
import { Image, Text, View, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface Props {
  category: any
  onPress: any
  index: any
}

export default function MenuListItem(props: Props) {
  const {
    category: { name, image, _id },
    onPress,
    index,
  } = props

  const renderWrapperimageStyle = () => {
    if (index === _id) {
      return {
        ...styles.wrapperImage,
        backgroundColor: '#f49999',
      }
    }
    return styles.wrapperImage
  }

  return (
    <TouchableOpacity onPress={() => onPress(_id)}>
      <View style={styles.container}>
        <View style={renderWrapperimageStyle()}>
          <Image
            style={styles.img}
            source={{
              uri: image,
            }}
          />
        </View>

        <Text style={styles.name}>{name}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    padding: 8,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    height: 32,
    width: 32,
    borderRadius: 48,
    marginBottom: 4,
    position: 'absolute',
    top: 8,
    left: 8,
  },
  name: {
    fontSize: 14,
  },
  wrapperImage: {
    width: 48,
    height: 48,
    backgroundColor: '#f0f0f0',
    position: 'relative',
    borderRadius: 48,
  },
})
