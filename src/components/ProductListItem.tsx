import React, { useState, useEffect } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import _ from 'lodash'

import { addToCart, removeFromCart } from '../actions/cart'

const ProductListItem = (props: any) => {
  const { data, cart, addToCart, removeFromCart } = props
  const [amount, setAmount] = useState(0)

  useEffect(() => {
    const index = _.findIndex(
      cart,
      ({ product }) => product && product._id === data._id
    )
    if (!!cart.length && index !== -1) {
      setAmount(cart[index].quantity)
    } else {
      setAmount(0)
    }
  }, [cart])

  return (
    <View style={styles.container}>
      <View>
        <Image source={{ uri: data.image }} style={styles.img} />
      </View>
      <View style={styles.info}>
        <Text style={styles.name}>{data.name}</Text>
        <Text style={styles.price}>{data.price}đ</Text>
      </View>
      <View style={styles.events}>
        {amount !== 0 && (
          <>
            <TouchableOpacity onPress={() => removeFromCart(data)}>
              <Icon name='minus-circle' size={30} color='#f43100' />
            </TouchableOpacity>
            <Text style={styles.quantity}>{amount}</Text>
          </>
        )}
        <TouchableOpacity onPress={() => addToCart(data)}>
          <Icon name='plus-circle' size={30} color='#f43100' />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    // padding: 8,
    margin: 8,
    backgroundColor: '#fff',
    // shadowColor: '#000',
    // shadowOpacity: 0.1,
    // shadowRadius: 10,
    // shadowOffset: { height: 0, width: 0 },
  },
  img: {
    width: 80,
    height: 80,
  },
  info: {
    flex: 1,
    marginLeft: 8,
    display: 'flex',
    padding: 8,
  },
  quantity: {
    paddingHorizontal: 8,
  },
  name: {
    fontSize: 16,
    fontWeight: '600',
    paddingBottom: 8,
    flex: 1,
  },
  price: {
    color: '#404040',
  },
  events: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 8,
  },
})

const mapStateToProps = (state: any) => {
  return {
    cart: state.cart,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    addToCart: (product: any) => dispatch(addToCart(product)),
    removeFromCart: (product: any) => dispatch(removeFromCart(product)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListItem)
