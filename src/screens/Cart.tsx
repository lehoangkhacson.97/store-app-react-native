import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Alert } from 'react-native'
import { connect } from 'react-redux'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { addToCart, removeFromCart } from '../actions/cart'
import CartListItem from '../components/CartListItem'

const Cart = (props: any) => {
  const { cart, addToCart, removeFromCart } = props
  const [total, setTotal] = useState(0)

  useEffect(() => {
    if (!!cart.length) {
      const result = cart.reduce((total, { quantity, product }) => {
        return total + quantity * parseInt(product.price)
      }, 0)
      setTotal(result)
    } else {
      setTotal(0)
    }
  }, [cart])

  const handlePay = () => {
    // removeAllFromCart()
    Alert.alert('Thanh toán thành công!')
  }

  return (
    <View style={styles.container}>
      <View style={styles.categories}>
        {!!cart.length &&
          cart.map((item: any, index: number) => (
            <CartListItem
              key={index}
              product={item.product}
              addToCart={addToCart}
              removeFromCart={removeFromCart}
              quantity={item.quantity}
            />
          ))}
          {!cart.length && <Text style={styles.empty}>Chưa có sản phẩm</Text>}
      </View>
      <View style={styles.total}>
        <Text style={styles.price}>{!!total ? `${total} K` : ''}</Text>
        <TouchableOpacity onPress={handlePay}>
          <Text style={styles.pay}>Đặt Hàng</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: '100%',
    backgroundColor: '#fff'
  },
  categories: {},
  total: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 8,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: '#fff',
    padding: 8,
    // shadowColor: '#000',
    // shadowRadius: 10,
    // shadowOpacity: 0.1,
    // shadowOffset: { height: 0, width: 0 },
  },
  price: {
    flex: 1,
    fontSize: 18,
    fontWeight: '600',
  },
  pay: {
    backgroundColor: '#f43100',
    padding: 16,
    fontSize: 18,
    fontWeight: '600',
    color: '#fff',
    borderRadius: 4,
  },
  empty: {
    textAlign: 'center',
    paddingTop: 16,
    fontSize: 16,
    fontWeight: '600'
  }
})

const mapStateToProps = (state: any) => {
  return {
    cart: state.cart,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    addToCart: (product: any) => dispatch(addToCart(product)),
    removeFromCart: (product: any) => dispatch(removeFromCart(product)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
