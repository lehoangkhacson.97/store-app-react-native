import axios from 'axios'
import {
  GET_CATEGORIES,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_ERROR,
} from '../constants'

export const getCategories = () => (dispatch: any) => {
  dispatch({
    type: GET_CATEGORIES,
  })

  // const request = axios({
  //   method: 'GET',
  //   url: `http://localhost:8080/products?restaurantId=${restaurantId}`,
  //   headers: {
  //     authorization:
  //       'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZTg4MGZlNjNlN2RiMDFjZGQ0NWIzNzciLCJpYXQiOjE1ODU5NzUyNzB9.lXD5APw3BY1S5rjdX4Sc0dnHGkk3g2urqonN8eg4_V4',
  //   },
  // })

  return axios
    .get(`https://buy-now-server.herokuapp.com/categories`, {
      headers: {
        authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZTkzNDgwM2IxMzE3ODAwMTdmM2U2NGIiLCJpYXQiOjE1ODY3MTA1MzN9.DbDMG8iWWa4-b3jS7AuOwOtqHTNcjXAaoyt9H-Ap0AY',
      },
    })
    .then((res: any) => {
      dispatch({
        type: GET_CATEGORIES_SUCCESS,
        payload: {
          data: res.data,
        },
      })
    })
    .catch((err: any) => {
      dispatch({
        type: GET_CATEGORIES_ERROR,
        payload: err,
      })
    })
}
