import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import SettingItem from '../components/SettingItem'

export default function Settings() {
  return (
    <View style={styles.container}>
      <View style={styles.info}>
        {/* <Image source={{}} */}
        <View style={styles.avatar}>
          <Text style={styles.avatarText}>N</Text>
        </View>
        <Text style={styles.name}>Lê Hoàng Khắc Sơn</Text>
      </View>
      <View style={styles.option}>
        <SettingItem text='Thanh toán' icon='pay' />
        <SettingItem text='Địa chỉ' icon='address' />
        <SettingItem text='Buynow xu' icon='coin' />
        <SettingItem text='Cửa hàng' icon='for-you' />
        <SettingItem text='Mời bạn bè' icon='friend' />
        <SettingItem text='Ví' icon='wallet' />
        <SettingItem text='Về Buynow' icon='for-we' />
        <SettingItem text='Cài đặt' icon='setting' />
      </View>
      <View style={styles.footer}>
        <Text style={styles.logout}>Đăng xuất</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {},
  info: {
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 32,
    marginBottom: 16
  },
  avatar: {
    height: 64,
    width: 64,
    backgroundColor: '#f43100',
    borderRadius: 64,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 12
  },
  avatarText: {
    color: '#fff',
    fontSize: 32,
    fontWeight: '700',
  },
  name: {
    fontWeight: '600',
    fontSize: 18
  },
  option: {},
  footer: {
    alignItems: 'center',
    borderColor: '#e37434',
    borderWidth: 1,
    paddingVertical: 8,
    marginHorizontal: 8,
    marginTop: 16,
    borderRadius: 24
  },
  logout: {
    color: '#e37434'
  }
})
