import React from 'react'
import axois from 'axios'
import _ from 'lodash'
import { StyleSheet } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { Provider } from 'react-redux'

import store from './src/store'

import AppNavigator from './AppNavigator'
import AppContext from './AppContext'

axois.defaults.baseURL = 'http://localhost:3000'

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <AppNavigator />
      </NavigationContainer>
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
})
