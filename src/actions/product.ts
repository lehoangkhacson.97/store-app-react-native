import axios from 'axios'
import {
  GET_PRODUCT,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_ERROR,
} from '../constants'

export const getProduct = (restaurantId: string) => (dispatch: any) => {
  dispatch({
    type: GET_PRODUCT,
  })

  // const request = axios({
  //   method: 'GET',
  //   url: `http://localhost:8080/products?restaurantId=${restaurantId}`,
  //   headers: {
  //     authorization:
  //       'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZTg4MGZlNjNlN2RiMDFjZGQ0NWIzNzciLCJpYXQiOjE1ODU5NzUyNzB9.lXD5APw3BY1S5rjdX4Sc0dnHGkk3g2urqonN8eg4_V4',
  //   },
  // })

  return axios
    .get(`https://buy-now-server.herokuapp.com/products?restaurantId=${restaurantId}`, {
      headers: {
        authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZTkzNDgwM2IxMzE3ODAwMTdmM2U2NGIiLCJpYXQiOjE1ODY3MTA1MzN9.DbDMG8iWWa4-b3jS7AuOwOtqHTNcjXAaoyt9H-Ap0AY',
      },
    })
    .then((res: any) => {
      dispatch({
        type: GET_PRODUCT_SUCCESS,
        payload: {
          data: res.data,
        },
      })
    })
    .catch((err: any) => {
      dispatch({
        type: GET_PRODUCT_ERROR,
        payload: err,
      })
    })
}
