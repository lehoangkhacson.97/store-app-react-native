import _ from 'lodash'

import { ADD_TO_CART, REMOVE_FROM_CART } from '../constants'

const initialState = []

const findIndex = (product: any, state: any) => {
  const { _id } = product
  if (!state.length) {
    return -1
  }
  return _.findIndex(state, ({ product }: any) => product._id === _id)
}

export default function reducer(state = initialState, actions: any) {
  switch (actions.type) {
    case ADD_TO_CART: {
      const { product } = actions.payload
      const index = findIndex(product, state)

      if (index === -1) {
        return [
          ...state,
          {
            product,
            quantity: 1,
          },
        ]
      }
      return [
        ...state.slice(0, index),
        {
          ...state[index],
          quantity: state[index].quantity + 1,
        },
        ...state.slice(index + 1),
      ]
    }
    case REMOVE_FROM_CART: {
      const { product } = actions.payload
      const index = findIndex(product, state)

      if (index === -1) {
        return state
      }
      if (state[index].quantity === 1) {
        return [...state.slice(0, index), ...state.slice(index + 1)]
      }
      return [
        ...state.slice(0, index),
        {
          ...state[index],
          quantity: state[index].quantity - 1,
        },
        ...state.slice(index + 1),
      ]
    }
    default:
      return state
  }
}
