import React, { useState, useEffect } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface Props {
  product: any
  addToCart: any
  removeFromCart: any
  quantity: number
}

export default function CartListItem(props: Props) {
  const { product, addToCart, removeFromCart, quantity } = props
  const [isShow, setIsShow] = useState(false)

  useEffect(() => {
    if (product || quantity < 1) {
      setIsShow(true)
    }
  }, [product])
  const renderUI = () => {
    if(isShow) {
      return (
        <View style={styles.container}>
        <View>
          <Image source={{uri: product.image}} style={styles.img} />
        </View>
        <View style={styles.info}>
          <Text style={styles.name}>{product.name}</Text>
          <Text style={styles.price}>{product.price}</Text>
        </View>
        <View style={styles.events}>
          <TouchableOpacity onPress={() => removeFromCart(product)}>
            <Icon name='minus-circle' size={30} color='#f43100' />
          </TouchableOpacity>
          <Text style={styles.quantity}>{quantity}</Text>
          <TouchableOpacity onPress={() => addToCart(product)}>
            <Icon name='plus-circle' size={30} color='#f43100' />
          </TouchableOpacity>
        </View>
      </View>
      )
    } else {
      return null
    }
  }

  return renderUI()
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 8,
    backgroundColor: '#fff',
    // shadowColor: '#000',
    // shadowOpacity: 0.1,
    // shadowRadius: 10,
    // shadowOffset: { height: 0, width: 0 }
  },
  img: {
    width: 80,
    height: 80
  },
  info: {
    flex: 1,
    marginLeft: 8
  },
  quantity: {
    paddingHorizontal: 8
  },
  name: {
    fontSize: 16,
    fontWeight: '600',
    paddingBottom: 8,
    textTransform: 'uppercase'
  },
  price: {
    color: '#404040'
  },
  events: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  }
})
