import {
  GET_CATEGORIES,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_ERROR,
} from '../constants'

const initialState: any = {
  loading: false,
  data: [],
  error: '',
}

export default function reducer(state = initialState, actions: any) {
  switch (actions.type) {
    case GET_CATEGORIES: {
      return {
        ...initialState,
        loading: true,
      }
    }
    case GET_CATEGORIES_SUCCESS: {
      const { data } = actions.payload
      return {
        ...initialState,
        data,
        loading: false,
      }
    }
    case GET_CATEGORIES_ERROR: {
      return {
        ...initialState,
        loading: false,
        error: actions.payload,
      }
    }
    default:
      return state
  }
}
