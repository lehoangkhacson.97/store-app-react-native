import React, { useState, useEffect } from 'react'
import { StyleSheet, FlatList, View, TextInput, Image } from 'react-native'
import { connect } from 'react-redux'
import { Carousel } from '@ant-design/react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import CategoryListItem from '../components/CategoryListItem'
import MenuListItem from '../components/MenuListItem'
import { getCategories } from '../actions/category'
import { getRestaurants } from '../actions/restaurant'

const Categories = (props: any) => {
  const {
    navigation,
    categories,
    restaurants,
    getCategories,
    getRestaurants,
  } = props
  const [category, setCategory] = useState(0)

  useEffect(() => {
    getRestaurants()
  }, [])

  useEffect(() => {
    getCategories()
  }, [])

  const handlePress = (item: any) => () => {
    navigation.navigate('Category', {
      restaurant: item,
    })
  }

  const handlePressCategory = (id: any) => {
    setCategory(id)
  }

  return (
    <View style={styles.container}>
      <View style={styles.wrapperSearchBar}>
        <Icon style={styles.searchIcon} name='search' color='#a0a0a0' />
        <TextInput style={styles.searchBar} placeholder='Tìm kiếm món ăn' />
      </View>
      <Carousel autoplay selectedIndex={2} infinite>
        <Image
          source={{
            uri:
              'https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/AN_images/healthy-eating-ingredients-1296x728-header.jpg?w=1155&h=1528',
          }}
          style={{ height: 120 }}
        />
        <Image
          source={{
            uri:
              'https://images.foody.vn/res/g88/876290/prof/s280x175/foody-upload-api-foody-mobile-bun-cha-ha-noi-thom--190104165139.jpg',
          }}
          style={{ height: 120 }}
        />
        <Image
          source={{
            uri:
              'https://images.foody.vn/res/g98/976905/prof/s280x175/foody-upload-api-foody-mobile-foody-upload-api-foo-191107194027.jpg',
          }}
          style={{ height: 120 }}
        />
      </Carousel>

      <FlatList
        data={categories.data}
        renderItem={({ item }: any) => (
          <MenuListItem
            category={item}
            index={category}
            onPress={handlePressCategory}
          />
        )}
        contentContainerStyle={styles.listMenu}
        horizontal={true}
        keyExtractor={(item: any) => item._id}
        showsHorizontalScrollIndicator={false}
      />
      <FlatList
        data={restaurants.data}
        numColumns={2}
        renderItem={({ item }) => (
          <CategoryListItem restaurant={item} onPress={handlePress(item)} />
        )}
        contentContainerStyle={styles.categories}
        keyExtractor={(item: any) => item._id}
        columnWrapperStyle={{
          display: 'flex',
          justifyContent: 'space-between',
        }}
        showsVerticalScrollIndicator={false}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    // flex: 1,
    backgroundColor: '#fff',
  },
  categories: {
    paddingHorizontal: 16,
    paddingTop: 16,
    // flex: 1,
    backgroundColor: '#fff',
  },
  listMenu: {
    height: 100
  },
  wrapperSearchBar: {
    backgroundColor: '#fff',
    paddingTop: 8,
    paddingBottom: 24,
    paddingHorizontal: 16,
    position: 'relative',
  },
  searchIcon: {
    position: 'absolute',
    top: 21,
    left: 25,
    zIndex: 10,
  },
  searchBar: {
    height: 40,
    paddingHorizontal: 24,
    paddingVertical: 8,
    borderRadius: 40,
    backgroundColor: '#f0f0f0',
  },
})

const mapStateToProps = (state: any) => {
  return {
    categories: state.category,
    restaurants: state.restaurant,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    getCategories: () => dispatch(getCategories()),
    getRestaurants: (category?: any) => dispatch(getRestaurants(category)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories)
