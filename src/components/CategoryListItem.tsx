import React from 'react'
import {
  Dimensions,
  Image,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

interface Props {
  restaurant: any
  onPress: any
}

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

export default function CategoryListItem(props: Props) {
  const {
    restaurant: { name, image, address },
    onPress,
  } = props
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={onPress}>
      <View style={styles.container}>
        <View>
          <Image source={{ uri: image }} style={styles.categoryImage} />
        </View>
        <View style={styles.info}>
          <Text style={styles.title} numberOfLines={1}>
            {name}
          </Text>
          <Text style={styles.address}>{address}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    // alignItems: 'center',
    // padding: 16,
    // margin: 16,
    borderRadius: 4,
    backgroundColor: '#fff',
    // shadowColor: '#000',
    // shadowOpacity: 0.3,
    // shadowRadius: 10,
    shadowOffset: { width: 0, height: 0 },
    marginBottom: 16,
    // flexDirection: 'row',
    width: windowWidth * 0.45,
    flex: 1,
  },
  title: {
    marginBottom: 8,
    fontWeight: '600',
  },
  categoryImage: {
    height: 120,
    marginRight: 8,
    marginBottom: 8,
  },
  address: {
    fontSize: 12,
    color: '#808080',
  },
  info: {
    paddingHorizontal: 8,
    paddingBottom: 8,
  },
})
