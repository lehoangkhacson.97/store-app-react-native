import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

interface Props {
  text: string
  icon:
    | 'wallet'
    | 'coin'
    | 'pay'
    | 'address'
    | 'friend'
    | 'for-you'
    | 'setting'
    | 'for-we'
}

export default function SettingItem(props: Props) {
  const { text, icon } = props

  const renderIcon = (value: string) => {
    switch(value) {
      case 'wallet':
        return 'amazon'
      case 'coin':
        return 'bitcoin'
      case 'pay':
        return 'credit-card'
      case 'address':
        return 'address-book'
      case 'friend':
        return 'user'
      case 'for-you':
        return 'archive'
      case 'setting':
        return 'cog'
      case 'for-we':
        return 'exclamation-circle'
    }
  }
  return (
    <View style={styles.container}>
      <Icon name={renderIcon(icon)} size={16} />
      <Text style={styles.text}>{text}</Text>
      <Icon name='chevron-right' size={16} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 12,
    marginBottom: 4,
  },
  text: {
    marginLeft: 8,
    flex: 1
  },
})
