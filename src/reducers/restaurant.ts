import {
  GET_RESTAURANTS,
  GET_RESTAURANTS_SUCCESS,
  GET_RESTAURANTS_ERROR,
} from '../constants'

const initialState: any = {
  loading: false,
  data: [],
  error: '',
}

export default function reducer(state = initialState, actions: any) {
  switch (actions.type) {
    case GET_RESTAURANTS: {
      return {
        ...initialState,
        loading: true,
      }
    }
    case GET_RESTAURANTS_SUCCESS: {
      const { data } = actions.payload
      return {
        ...initialState,
        data,
        loading: false,
      }
    }
    case GET_RESTAURANTS_ERROR: {
      return {
        ...initialState,
        loading: false,
        error: actions.payload,
      }
    }
    default:
      return state
  }
}
